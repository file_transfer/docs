# Message Bus Contracts

This section documents the schema for the messages sent over NATS.

## Topic `workers.queue`

```json
{
   "$schema": "http://json-schema.org/draft-07/schema#",
   "Title": "workers.queue",
   "description": "This topic is used by the dispatcher to notify workers in the queue that an action is ready to be taken on a file.",
   "properties": {
      "action": {
         "description": "Action the dispatcher is sending to workers.",
         "type": "string",
         "enum": ["fileUpload"]
      },
      "file": {
         "description": "Name of the file in the watched directory.",
         "type": "string"
      }
   }
}
```

## Topic `workers.status`

```json
{
   "$schema": "http://json-schema.org/draft-07/schema#",
   "Title": "workers.status",
   "description": "This topic is used by the workers to inform the dispatcher of updates to file actions.",
   "properties": {
      "action": {
         "description": "Action or update notification the worker is sending.",
         "type": "string",
         "enum": ["uploadStart", "uploadProgress", "uploadDone", "uploadError"]
      },
      "file": {
         "description": "Name of the file in the watched directory.",
         "type": "string"
      },
      "worker": {
         "description": "Hostname of the worker sending the message.",
         "type": "string"
      },
      "meta": {
         "description": "Metadata associated with the message, such as the percentage uploaded, error message, etc.",
         "type": "string"
      }
   }
}
```
