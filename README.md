# File Transfer Demo, by Eric Flores 

This project is a file transfer application that monitors an NFS drive and when a new file is detected, it uploads the file to AWS S3. 

While the use case is fictitious, it is designed to be a demo of my skills with the following technologies:

* Golang
* Node.js 
* NATS messaging server
* Docker
* Terraform
* AWS
* CI/CD
* DevOps

## Major systems

Each of the major microservices are broken up into separate repos and serve the following purposes:

* Dispatcher - This repo is a Node.js based microservice that monitors the NFS drive looking for new files. When it detects one, it dispatches a worker to handle the file upload with a message over NATS.
* Worker - This repo is a Go based microservice that handles the actual file uploads. It then notifies the dispatcher with status updates over NATS.
* NATS - This purpose of this repo is to add a custom configuration to the official NATS container and run that config through tests.
 
## Infrastructure

All of the infrustucture runs on AWS and is provisioned on demand from Slack chatops commands that trigger Gitlab CI jobs. These jobs run terraform to setup all the servers, DNS records, and containers.

The app runs on Ubuntu EC2 servers running Docker Swarm. The NFS drive that is monitored is an AWS EFS instance.

The runners for Gitlab CI run on a stand alone docker host. When a container is built in CI, the image is pushed up to a private registry on that host. These containers are then pulled but the swarm cluster as needed.

If you would like to see a demo of this app, please contact me.

## Other docs

Message bus contract docs: [here](./MessageBus/) - Documents the schema of the message sent over NATS

<img class="statcounter" src="https://c.statcounter.com/11994331/0/eed1a29e/1/">
